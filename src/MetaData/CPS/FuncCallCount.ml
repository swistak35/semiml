let table = Hashtbl.create 1024

let raise_already_added name =
  let err = Printf.sprintf "MetaData::CPS::FuncCallCount: function %s already added" (Common.Var.string_of_var name)
  in begin
    CompilerLog.error "%s" err;
    failwith err;
  end

let raise_not_found name =
  let err = Printf.sprintf "MetaData::CPS::FuncCallCount: variable %s not found" (Common.Var.string_of_var name)
  in begin
    CompilerLog.error "%s" err;
    failwith err;
  end

let get name = 
  try Hashtbl.find table name with
  | Not_found -> raise_not_found name

let add name =
  if Hashtbl.mem table name
  then raise_already_added name
  else Hashtbl.add table name 0

let get_with_default name =
  try (Hashtbl.find table name) with
  | Not_found -> 0

let inc name =
  let previousCount = get_with_default name
  in Hashtbl.replace table name (previousCount + 1)
