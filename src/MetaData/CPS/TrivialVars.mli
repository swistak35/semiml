val get     : Common.Var.t -> bool
val try_get : Common.Var.t -> bool option

val set     : Common.Var.t -> bool -> unit
