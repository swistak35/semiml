
val sml_string_of_int    : int -> string
val sml_string_of_int64  : int64 -> string
val sml_string_of_string : string -> string
