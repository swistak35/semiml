
type t = int64

let next_fresh_tag = ref 0L
let fresh () =
  let r = !next_fresh_tag in
  next_fresh_tag := Int64.succ r;
  r
