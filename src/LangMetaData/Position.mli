
val get     : Common.Tag.t -> Common.Position.t
val try_get : Common.Tag.t -> Common.Position.t option

val set : Common.Tag.t -> Common.Position.t -> unit

val copy : Common.Tag.t -> Common.Tag.t -> unit
