type t =
| MetaCmd_Eval
| MetaCmd_Pretty
| MetaCmd_Require of string list

val run : Compiler.state -> t -> Compiler.state
