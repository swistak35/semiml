
val register_analysis : Analysis.t -> unit

val possible_analyses : 
  'lang Types.language -> Contract.Set.t -> Analysis.t list
