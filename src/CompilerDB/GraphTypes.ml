
type node =
| Node : 'lang Types.language * Contract.Set.t -> node

type command =
| Cmd_Analyse   of Analysis.t
| Cmd_Eval      of Evaluator.t
| Cmd_Pretty    of Pretty.t
| Cmd_Transform of Transform.t

type path = command list

module Node = struct
  type t = node

  let compare node1 node2 =
    match node1, node2 with
    | Node(lang1, contr1), Node(lang2, contr2) ->
      let l = Types.Language.compare lang1 lang2 in
      if l = 0 then Contract.Set.compare contr1 contr2
      else l

  let equal node1 node2 =
    compare node1 node2 = 0

  let instance node1 node2 =
    match node1, node2 with
    | Node(lang1, contr1), Node(lang2, contr2) ->
      Types.Language.equal lang1 lang2 && Contract.Set.subset contr2 contr1
end
