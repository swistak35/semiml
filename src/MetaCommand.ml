module Box = Printing.Box

type t =
| MetaCmd_Eval
| MetaCmd_Pretty
| MetaCmd_Require of string list

let run state mc =
  let node = CompilerGraph.node_of_state state in
  match mc with
  | MetaCmd_Eval ->
    begin match CompilerGraph.find_path_to_eval node with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Cannot evaluate program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ];
      raise Errors.Fatal_error
    end
  | MetaCmd_Pretty ->
    begin match CompilerGraph.find_path_to_pretty node with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Cannot pretty-print program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ];
      raise Errors.Fatal_error
    end
  | MetaCmd_Require contracts ->
    begin match CompilerGraph.find_path_to_contract_names node contracts with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ; Box.white_sep (Box.text "cannot simultaneously satisfy contract(s):")
      ; Box.indent 2 (Box.box 
          (List.map (fun c -> Box.white_sep (Box.text c)) contracts))
      ];
      raise Errors.Fatal_error
    end
