module TV = TypeView

type t =
| Var       of Type.tvar
| Abstract  of TV.t list * Type.tcon
| Arrow     of TV.t * TV.t
| Record    of TV.t list
| DataType
    of TypeView.t list
     * Type.tvar list
     * Type.tcon
     * (Type.cname * Type.t option) list
| LocalDataType 
    of (Type.tcon * TypeView.datatype_def) list
     * TypeView.t list
     * Type.tcon
     * TypeView.datatype_def
| ForallVar of TypeView.t TypeView.tvar_binder
| Forall    of TypeView.t TypeView.tcon_binder
| Exists    of TypeView.t TypeView.tcon_binder

let rec build_typedef env x def nf =
  match nf with
  | Var _ -> nf
  | Abstract(args, y) ->
    if Type.TCon.equal x y then v_nf env (TV.unfold_typedef args def)
    else Abstract(List.map (TV.define x def) args, y)
  | Arrow(tp1, tp2) -> 
    Arrow(TV.define x def tp1, TV.define x def tp2)
  | Record tps -> Record(List.map (TV.define x def) tps)
  | DataType(args, fargs, c, ctors) ->
    DataType(List.map (TV.define x def) args, fargs, c, ctors)
  | LocalDataType(ddefs, args, y, ddef) ->
    LocalDataType(
      List.map 
        (fun (z, ddef) -> (z, TV.define_in_datatype_def x def ddef))
        ddefs,
      List.map (TV.define x def) args,
      y,
      TV.define_in_datatype_def x def ddef)
  | ForallVar bind ->
    let (y, tp) = TV.open_tvar bind in
    ForallVar(TV.close_tvar y (TV.define x def tp))
  | Forall bind ->
    let (n, y, tp) = TV.open_tcon bind in
    Forall(TV.close_tcon n y (TV.define x def tp))
  | Exists bind ->
    let (n, y, tp) = TV.open_tcon bind in
    Exists(TV.close_tcon n y (TV.define x def tp))

and build_datatype defs nf =
  match nf with
  | Var _ -> nf
  | Abstract(args, y) ->
    begin match List.find (fun (x, _) -> Type.TCon.equal x y) defs with
    | (x, def) -> LocalDataType(defs, args, x, def)
    | exception Not_found ->
      let args = List.map (TV.datatype defs) args in
      Abstract(args, y)
    end
  | Arrow(tp1, tp2) ->
    Arrow(TV.datatype defs tp1, TV.datatype defs tp2)
  | Record tps -> Record(List.map (TV.datatype defs) tps)
  | DataType(args, fargs, c, ctors) ->
    DataType(List.map (TV.datatype defs) args, fargs, c, ctors)
  | LocalDataType(ddefs, args, y, ddef) ->
    LocalDataType(
      List.map 
        (fun (z, ddef) -> (z, TV.datatype_in_datatype_def defs ddef))
        ddefs,
      List.map (TV.datatype defs) args,
      y,
      TV.datatype_in_datatype_def defs ddef)
  | ForallVar bind ->
    let (y, tp) = TV.open_tvar bind in
    ForallVar(TV.close_tvar y (TV.datatype defs tp))
  | Forall bind ->
    let (n, y, tp) = TV.open_tcon bind in
    Forall(TV.close_tcon n y (TV.datatype defs tp))
  | Exists bind ->
    let (n, y, tp) = TV.open_tcon bind in
    Exists(TV.close_tcon n y (TV.datatype defs tp))

and v_nf env tp =
  match TV.view tp with
  | TV.Var x -> Var x
  | TV.TyCon(args, x) ->
    begin match TypeEnv.get_tcon env x with
    | TypeEnv.Abstract _ -> Abstract(args, x)
    | TypeEnv.TypeDef(fargs, body) ->
      let body =
        List.fold_left2 (fun body x tp ->
          TV.subst x tp body
        ) (TV.of_type body) fargs args in
      v_nf env body
    | TypeEnv.DataType(fargs, ctors) ->
      DataType(args, fargs, x, ctors)
    end
  | TV.Arrow(tp1, tp2) -> Arrow(tp1, tp2)
  | TV.Record tps      -> Record tps
  | TV.TypeDef(def, bind) ->
    let (n, x, tp) = TV.open_tcon bind in
    let env' = TypeEnv.add_fresh_tcon env x (TypeEnv.Abstract n) in
    build_typedef env x def (v_nf env' tp)
  | TV.DataType ldt -> datatype_nf env ldt
  | TV.ForallVar bind  -> ForallVar bind
  | TV.Forall bind -> Forall bind
  | TV.Exists bind -> Exists bind

and datatype_nf env ldt =
  match TV.view_local_datatype ldt with
  | TV.LDT_Abs bind ->
    let (n, x, ldt) = TV.open_tcon bind in
    let env = TypeEnv.add_fresh_tcon env x (TypeEnv.Abstract n) in
    datatype_nf env ldt
  | TV.LDT_DataType(defs, tp) ->
    build_datatype defs (v_nf env tp)

let nf env tp =
  v_nf env (TV.of_type tp)

let try_as_arrow env tp =
  match nf env tp with
  | Arrow(tp1, tp2) -> Some(TV.to_type tp1, TV.to_type tp2)
  | _ -> None

let try_as_record env tp =
  match nf env tp with
  | Record tps -> Some(List.map TV.to_type tps)
  | _ -> None

let try_as_forall_var env tp =
  match nf env tp with
  | ForallVar bind -> Some bind
  | _ -> None

let try_as_forall env tp =
  match nf env tp with
  | Forall bind -> Some bind
  | _ -> None

let try_as_exists env tp =
  match nf env tp with
  | Exists bind -> Some bind
  | _ -> None
