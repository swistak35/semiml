module TV = TypeView

type nf =
| Var       of Type.tvar
| Abstract  of TV.t list * Type.tcon
| Arrow     of TV.t * TV.t
| Record    of TV.t list
| DataType  of
    TV.t list * Type.tvar list * Type.tcon * (Type.cname * Type.t option) list
| ForallVar of TV.t TV.tvar_binder
| Forall    of TV.t TV.tcon_binder
| Exists    of TV.t TV.tcon_binder

let assumed assm x1 x2 =
  List.exists (fun (y1, y2) ->
    Type.TCon.equal x1 y1 && Type.TCon.equal x2 y2
  ) assm

let rec norm env tp =
  match TV.view tp with
  | TV.Var x -> (env, Var x)
  | TV.TyCon(args, x) ->
    begin match TypeEnv.get_tcon env x with
    | TypeEnv.Abstract _ -> (env, Abstract(args, x))
    | TypeEnv.TypeDef(fargs, body) ->
      let body =
        List.fold_left2 (fun body x tp ->
          TV.subst x tp body
        ) (TV.of_type body) fargs args in
      norm env body
    | TypeEnv.DataType(fargs, ctors) ->
      (env, DataType(args, fargs, x, ctors))
    end
  | TV.Arrow(tp1, tp2) -> (env, Arrow(tp1, tp2))
  | TV.Record tps      -> (env, Record tps)
  | TV.TypeDef(def, bind) ->
    let (_, x, tp) = TV.open_tcon bind in
    let (args, body) = TV.concrete_typedef def in
    let env = TypeEnv.add_fresh_tcon env x (TypeEnv.TypeDef(args, body)) in
    norm env tp
  | TV.DataType dt -> norm_datatype env dt
  | TV.ForallVar bind  -> (env, ForallVar bind)
  | TV.Forall bind -> (env, Forall bind)
  | TV.Exists bind -> (env, Exists bind)

and norm_datatype env dt =
  match TV.view_local_datatype dt with
  | TV.LDT_Abs bind ->
    let (n, x, dt) = TV.open_tcon bind in
    let env = TypeEnv.add_fresh_tcon env x (TypeEnv.Abstract n) in
    norm_datatype env dt
  | TV.LDT_DataType(defs, tp) ->
    let env = List.fold_left (fun env (x, def) ->
        let (args, ctors) = TV.concrete_datatype_def def in
        TypeEnv.upgrade_to_datatype env args x ctors
      ) env defs in
    norm env tp

let rec eq assm env1 env2 tp1 tp2 =
  let (env1, nf1) = norm env1 tp1 in
  let (env2, nf2) = norm env2 tp2 in
  match nf1, nf2 with
  | Var x1, Var x2 -> Type.TVar.equal x1 x2
  | Var _, 
    ( Abstract _ | Arrow _ | Record _ | DataType _ | ForallVar _ | Forall _ 
    | Exists _) -> false
  | Abstract(args1, x1), Abstract(args2, x2) ->
    Type.TCon.equal x1 x2 &&
    List.for_all2 (eq assm env1 env2) args1 args2
  | Abstract _, 
    ( Var _ | Arrow _ | Record _ | DataType _ | ForallVar _ | Forall _ 
    | Exists _) -> false
  | Arrow(ta1, tv1), Arrow(ta2, tv2) ->
    eq assm env1 env2 ta1 ta2 && eq assm env1 env2 tv1 tv2
  | Arrow _, 
    ( Var _ | Abstract _ | Record _ | DataType _ | ForallVar _ | Forall _ 
    | Exists _) -> false
  | Record tps1, Record tps2 ->
    List.length tps1 = List.length tps2 &&
    List.for_all2 (eq assm env1 env2) tps1 tps2
  | Record _,
    ( Var _ | Abstract _ | Arrow _ | DataType _ | ForallVar _ | Forall _ 
    | Exists _) -> false
  | DataType(args1, fargs1, x1, ctors1), DataType(args2, fargs2, x2, ctors2) ->
    if assumed assm x1 x2 then
      List.for_all2 (eq assm env1 env2) args1 args2
    else if List.length fargs1 <> List.length fargs2 then false
    else
      List.for_all2 (eq assm env1 env2) args1 args2 &&
      eq_datatypes ((x1, x2) :: assm) env1 env2 
        (TV.datatype_def fargs1 ctors1)
        (TV.datatype_def fargs2 ctors2)
  | DataType _,
    ( Var _ | Abstract _ | Arrow _ | Record _ | ForallVar _ | Forall _ 
    | Exists _) -> false
  | ForallVar bnd1, ForallVar bnd2 ->
    let (x, tp1) = TV.open_tvar bnd1 in
    let tp2 = TV.open_fresh_tvar x bnd2 in
    let env1 = TypeEnv.add_fresh_tvar env1 x in
    let env2 = TypeEnv.add_fresh_tvar env2 x in
    eq assm env1 env2 tp1 tp2
  | ForallVar _, 
    ( Var _ | Abstract _ | Arrow _ | Record _ | DataType _ | Forall _ 
    | Exists _) -> false
  | Forall bnd1, Forall bnd2 ->
    let (n1, x, tp1) = TV.open_tcon bnd1 in
    let (n2, tp2) = TV.open_fresh_tcon x bnd2 in
    if n1 <> n2 then false
    else begin
      let env1 = TypeEnv.add_fresh_tcon env1 x (TypeEnv.Abstract n1) in
      let env2 = TypeEnv.add_fresh_tcon env2 x (TypeEnv.Abstract n2) in
      eq assm env1 env2 tp1 tp2
    end
  | Forall _, 
    (Var _ | Abstract _ | Arrow _ | Record _ | DataType _ | ForallVar _ 
    | Exists _) -> false
  | Exists bnd1, Exists bnd2 ->
    let (n1, x, tp1) = TV.open_tcon bnd1 in
    let (n2, tp2) = TV.open_fresh_tcon x bnd2 in
    if n1 <> n2 then false
    else begin
      let env1 = TypeEnv.add_fresh_tcon env1 x (TypeEnv.Abstract n1) in
      let env2 = TypeEnv.add_fresh_tcon env2 x (TypeEnv.Abstract n2) in
      eq assm env1 env2 tp1 tp2
    end
  | Exists _, 
    (Var _ | Abstract _ | Arrow _ | Record _ | DataType _ | ForallVar _ 
    | Forall _) -> false

and eq_datatypes assm env1 env2 dt1 dt2 =
  match TV.view_datatype dt1, TV.view_datatype dt2 with
  | TV.DT_Abs bind1, TV.DT_Abs bind2 ->
    let (x, dt1) = TV.open_tvar bind1 in
    let dt2      = TV.open_fresh_tvar x bind2 in
    let env1 = TypeEnv.add_fresh_tvar env1 x in
    let env2 = TypeEnv.add_fresh_tvar env2 x in
    eq_datatypes assm env1 env2 dt1 dt2
  | TV.DT_Abs _, TV.DT_Ctors _ -> false
  | TV.DT_Ctors ctors1, TV.DT_Ctors ctors2 ->
    eq_datatype_ctors assm env1 env2 ctors1 ctors2
  | TV.DT_Ctors _, TV.DT_Abs _ -> false

and eq_datatype_ctors assm env1 env2 ctors1 ctors2 =
  match ctors1, ctors2 with
  | [], [] -> true
  | _ :: _, [] -> false
  | [], _ :: _ -> false
  | (_, None) :: ctors1, (_, None) :: ctors2 ->
    eq_datatype_ctors assm env1 env2 ctors1 ctors2
  | (_, None) :: _, (_, Some _) :: _ -> false
  | (_, Some tp1) :: ctors1, (_, Some tp2) :: ctors2 ->
    eq assm env1 env2 tp1 tp2 &&
    eq_datatype_ctors assm env1 env2 ctors1 ctors2
  | (_, Some _) :: _, (_, None) :: _ -> false

let v_equal env tp1 tp2 =
  eq [] env env tp1 tp2

let equal env tp1 tp2 =
  eq [] env env (TV.of_type tp1) (TV.of_type tp2)
