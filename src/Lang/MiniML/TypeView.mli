
type t

val of_type : Type.t -> t
val to_type : t -> Type.t

type typedef
type datatype_def
type local_datatype

val concrete_typedef : typedef -> Type.tvar list * Type.t
val unfold_typedef   : t list  -> typedef -> t

val datatype_def :
  Type.tvar list -> (Type.cname * Type.t option) list -> datatype_def
val concrete_datatype_def : 
  datatype_def -> Type.tvar list * (Type.cname * Type.t option) list

type 'a tvar_binder
type 'a tcon_binder

type datatype_view =
| DT_Abs   of datatype_def tvar_binder
| DT_Ctors of (Type.cname * t option) list

type local_datatype_view =
| LDT_Abs      of local_datatype tcon_binder
| LDT_DataType of (Type.tcon * datatype_def) list * t

type view =
| Var       of Type.tvar
| TyCon     of t list * Type.tcon
| Arrow     of t * t
| Record    of t list
| TypeDef   of typedef * t tcon_binder
| DataType  of local_datatype
| ForallVar of t tvar_binder
| Forall    of t tcon_binder
| Exists    of t tcon_binder

val open_tvar  : 'a tvar_binder -> Type.tvar * 'a
val close_tvar : Type.tvar -> t -> t tvar_binder

(** in [open_fresh_tvar x binder] vairable [x] must be fresh *)
val open_fresh_tvar : Type.tvar -> 'a tvar_binder -> 'a

val open_tcon  : 'a tcon_binder -> int * Type.tcon * 'a
val close_tcon : int -> Type.tcon -> t -> t tcon_binder

(** in [open_fresh_tcon x binder] vairable [x] must be fresh *)
val open_fresh_tcon : Type.tcon -> 'a tcon_binder -> int * 'a

val view_datatype : datatype_def -> datatype_view

val view_local_datatype : local_datatype -> local_datatype_view

val view : t -> view

val subst : Type.tvar -> t -> t -> t

val define : Type.tcon -> typedef -> t -> t
val define_in_datatype_def :
  Type.tcon -> typedef -> datatype_def -> datatype_def

val datatype : (Type.tcon * datatype_def) list -> t -> t
val datatype_in_datatype_def :
  (Type.tcon * datatype_def) list -> datatype_def -> datatype_def
