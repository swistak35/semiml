open Lang.CPS.Ast

let bound = 100

let rec loop n e = 
  if n > bound then e else
  let e1 = BetaExpansion.transform e in
  if e1 = e then e1 else
  let e2 = Hoisting.transform e1 in
  let e3 = CSE.transform e2 in
  loop (n+1) e3

let transform expr = loop 0 expr

let c_meta = Contract.create
  ~description: "Meta-transformation responsible for invoking others"
  ~languages: [Language.CPS]
  "transform:meta"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:meta"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ; c_meta
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Analysis.CPS.FunName2Def.contract
      ; Contract.saves_contract Analysis.CPS.FuncCallCount.contract
      ; Contract.saves_contract Analysis.CPS.Var2Def.contract
      ]
    transform
