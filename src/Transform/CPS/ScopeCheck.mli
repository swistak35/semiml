
val check : Lang.RawCPS.Ast.expr -> Lang.CPS.Ast.expr
val c_scope_check : Contract.t

val register : unit -> unit
