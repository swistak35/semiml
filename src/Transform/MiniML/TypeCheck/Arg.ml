module S  = Lang.RawMiniML.Ast
module T  = Lang.MiniML.Ast
module TT = Lang.MiniML.Type
module TV = Lang.MiniML.TypeView
module Box = Printing.Box

type expr_type = T.expr * TT.t

let mk_expr tag kind =
  { T.e_tag  = tag
  ; T.e_kind = kind
  }

let rec add_vars tag env xs tp =
  match xs with
  | []      -> (env, (fun e -> e), (fun t -> t))
  | x :: xs ->
    let (env, x)        = Env.add_var env x.S.id_name tp in
    let (env, mke, mkt) = add_vars tag env xs tp in
    ( env 
    , (fun e -> mk_expr tag (T.Fn(x, tp, mke e)))
    , (fun t -> TT.Arrow(tp, mkt t))
    )

let rec check_vars tag env xs atp tp =
  match xs with
  | [] -> (env, (fun e -> e), tp)
  | x :: xs ->
    let (atp', tp) =
      match Env.try_type_as_arrow env tp with
      | None ->
        let scope = Env.type_scope env in
        Errors.error_b ~tag: x.S.id_tag
          [ Box.text "Abstracting a value of type"
          ; Box.indent 2 (Box.white_sep 
              (Lang.MiniML.Pretty.pretty_type scope 0 atp))
          ; Box.white_sep (Box.text "in the context of type")
          ; Box.indent 2 (Box.white_sep 
              (Lang.MiniML.Pretty.pretty_type scope 0 tp))
          ];
        raise Errors.Fatal_error
      | Some p -> p
    in
    if not (Env.type_equal env atp atp') then begin
      let scope = Env.type_scope env in
      Errors.error_b ~tag: x.S.id_tag
        [ Box.text "This variable has type"
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 atp))
        ; Box.white_sep (Box.text "but a variable was expected of type")
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 atp'))
        ];
      raise Errors.Fatal_error
    end;
    let (env, x)       = Env.add_var env x.S.id_name atp in
    let (env, mke, tp) = check_vars tag env xs atp tp in
    ( env 
    , (fun e -> mk_expr tag (T.Fn(x, atp, mke e)))
    , tp
    )

let mk_type_var_fn tag var_tag x body =
  if T.Expr.is_value body then mk_expr tag (T.TypeVarFn(x, body))
  else begin
    Errors.error ~tag: var_tag
      "Abstracting type in a non-value expression (value restriction).";
    raise Errors.Fatal_error
  end

let mk_type_fn tag tp_tag n x body =
  if T.Expr.is_value body then mk_expr tag (T.TypeFn(n, x, body))
  else begin
    Errors.error ~tag: tp_tag
      "Abstracting type in a non-value expression (value restriction).";
    raise Errors.Fatal_error
  end

let rec add_type_vars tag env xs =
  match xs with
  | [] -> (env, (fun e -> e), (fun t -> t))
  | x :: xs ->
    let var_tag = x.S.id_tag in
    let (env, x) = Env.add_tvar env x.S.id_name in
    let (env, mke, mkt) = add_type_vars tag env xs in
    ( env
    , (fun e -> mk_type_var_fn tag var_tag x (mke e))
    , (fun t -> TT.ForallVar(x, mkt t))
    )

let rec check_type_vars tag env xs tp =
  match xs with
  | [] -> (env, (fun e -> e), tp)
  | x :: xs ->
    let var_tag = x.S.id_tag in
    let bind =
      match Env.try_type_as_forall_var env tp with
      | None ->
        let scope = Env.type_scope env in
        Errors.error_b ~tag: x.S.id_tag
          [ Box.text "Abstracting a type in the context of type"
          ; Box.indent 2 (Box.white_sep
              (Lang.MiniML.Pretty.pretty_type scope 0 tp))
          ];
        raise Errors.Fatal_error
      | Some bind -> bind
    in
    let (env, x) = Env.add_tvar env x.S.id_name in
    let tp = TV.to_type (TV.open_fresh_tvar x bind) in
    let (env, mke, tp) = check_type_vars tag env xs tp in
    ( env
    , (fun e -> mk_type_var_fn tag var_tag x (mke e))
    , tp
    )

let rec add_type_decls tag env decls =
  match decls with
  | [] -> (env, (fun e -> e), (fun t -> t))
  | decl :: decls ->
    let n = List.length decl.S.tdecl_args in
    let (env, x) = Env.add_abstype env decl.S.tdecl_name.S.id_name n in
    let (env, mke, mkt) = add_type_decls tag env decls in
    ( env
    , (fun e -> mk_type_fn tag decl.S.tdecl_tag n x (mke e))
    , (fun t -> TT.Forall(n, x, mkt t))
    )

let rec check_type_decls tag env decls tp =
  match decls with
  | [] -> (env, (fun e -> e), tp)
  | decl :: decls ->
    let bind = 
      match Env.try_type_as_forall env tp with
      | None ->
        let scope = Env.type_scope env in
        Errors.error_b ~tag: decl.S.tdecl_tag
          [ Box.text "Abstracting a type in the context of type"
          ; Box.indent 2 (Box.white_sep
              (Lang.MiniML.Pretty.pretty_type scope 0 tp))
          ];
        raise Errors.Fatal_error
      | Some bind -> bind
    in
    let n = List.length decl.S.tdecl_args in
    let (env, x) = Env.add_abstype env decl.S.tdecl_name.S.id_name n in
    let (m, tp)  = TV.open_fresh_tcon x bind in
    let tp = TV.to_type tp in
    if n <> m then begin
      let scope = Env.type_scope env in
      Errors.error_b ~tag: decl.S.tdecl_tag
        [ Box.text (Printf.sprintf
            "Abstracting a type with %d parameter(s) in the context of type"
            n)
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp)) |> Box.br
        ; Box.text (Printf.sprintf
            "Abstracted type should have %d parameter(s)."
            m)
        ];
      raise Errors.Fatal_error
    end;
    let (env, mke, tp) = check_type_decls tag env decls tp in
    ( env
    , (fun e -> mk_type_fn tag decl.S.tdecl_tag n x (mke e))
    , tp
    )

let infer_type tag env arg =
  match arg.S.arg_kind with
  | S.Arg_Annot(xs, tp) ->
    let tp = Type.check env tp in
    add_vars tag env xs tp
  | S.Arg_Var x ->
    Errors.error ~tag: x.S.id_tag
      "Cannot infer type of this parameter.";
    raise Errors.Fatal_error
  | S.Arg_TypeVar xs ->
    add_type_vars tag env xs
  | S.Arg_Type decls ->
    add_type_decls tag env decls

let check_type tag env arg tp =
  match arg.S.arg_kind with
  | S.Arg_Annot(xs, atp) ->
    let atp = Type.check env atp in
    check_vars tag env xs atp tp
  | S.Arg_Var x ->
    let (atp, tp) =
      match Env.try_type_as_arrow env tp with
      | None ->
        let scope = Env.type_scope env in
        Errors.error_b ~tag: x.S.id_tag
          [ Box.text "Abstracting a value in the context of type"
          ; Box.indent 2 (Box.white_sep 
              (Lang.MiniML.Pretty.pretty_type scope 0 tp))
          ];
        raise Errors.Fatal_error
      | Some p -> p
    in
    let (env, x) = Env.add_var env x.S.id_name atp in
    ( env 
    , (fun e -> mk_expr tag (T.Fn(x, atp, e)))
    , tp
    )
  | S.Arg_TypeVar xs ->
    check_type_vars tag env xs tp
  | S.Arg_Type decls ->
    check_type_decls tag env decls tp

let rec infer_types tag env args =
  match args with
  | [] -> (env, (fun e -> e), (fun t -> t))
  | arg :: args ->
    let (env, mke1, mkt1) = infer_type  tag env arg in
    let (env, mke2, mkt2) = infer_types tag env args in
    (env, (fun e -> mke1 (mke2 e)), (fun t -> mkt1 (mkt2 t)))

let rec check_types tag env args tp =
  match args with
  | [] -> (env, (fun e -> e), tp)
  | arg :: args ->
    let (env, mke1, tp) = check_type  tag env arg tp in
    let (env, mke2, tp) = check_types tag env args tp in
    (env, (fun e -> mke1 (mke2 e)), tp)
