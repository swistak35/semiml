
type color_policy =
| NoColors
| AutoDetect
| ForceColors

type color_scheme

val set_color_policy : color_policy -> unit

val default_scheme : color_scheme
val dark_scheme    : color_scheme

val set_color_scheme : color_scheme -> unit

val use_colors : out_channel -> bool

val use_attributes     : out_channel -> DataTypes.attribute list -> unit
val default_attributes : out_channel -> unit
