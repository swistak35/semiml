open DataTypes

type color_policy =
| NoColors
| AutoDetect
| ForceColors

type color_scheme =
  { s_error       : base_attribute list
  ; s_keyword     : base_attribute list
  ; s_number      : base_attribute list
  ; s_literal     : base_attribute list
  ; s_constant    : base_attribute list
  ; s_constructor : base_attribute list
  ; s_operator    : base_attribute list
  ; s_paren       : base_attribute list
  ; s_variable    : base_attribute list
  }

let default_scheme =
  { s_error       = [ BA_FgColor Black; BA_BgColor Red ]
  ; s_keyword     = [ BA_Bold; BA_Underline; BA_FgColor Blue ]
  ; s_number      = [ BA_FgColor Red ]
  ; s_literal     = [ BA_FgColor Red ]
  ; s_constant    = [ BA_FgColor Cyan ]
  ; s_constructor = [ BA_FgColor Green; BA_Underline ]
  ; s_operator    = [ BA_FgColor Magenta ]
  ; s_paren       = [ BA_FgColor Magenta ]
  ; s_variable    = [ ]
  }

let dark_scheme =
  { s_error       = [ BA_FgColor Black; BA_BgColor Red ]
  ; s_keyword     = [ BA_Bold; BA_Underline; BA_FgColor Blue ]
  ; s_number      = [ BA_FgColor Red ]
  ; s_literal     = [ BA_FgColor Red ]
  ; s_constant    = [ BA_FgColor Cyan ]
  ; s_constructor = [ BA_FgColor Magenta; BA_Underline ]
  ; s_operator    = [ BA_FgColor Yellow ]
  ; s_paren       = [ BA_FgColor Yellow ]
  ; s_variable    = [ ]
  }

let color_policy = ref AutoDetect
let set_color_policy policy =
  color_policy := policy

let current_scheme = ref default_scheme
let set_color_scheme scheme =
  current_scheme := scheme

let use_colors chan =
  match !color_policy with
  | NoColors    -> false
  | AutoDetect  -> Unix.isatty (Unix.descr_of_out_channel chan)
  | ForceColors -> true

let color_code c =
  match c with
  | Black   -> 0
  | Red     -> 1
  | Green   -> 2
  | Yellow  -> 3
  | Blue    -> 4
  | Magenta -> 5
  | Cyan    -> 6
  | White   -> 7

let output_base_attr_code chan attr =
  match attr with
  | BA_Bold       -> output_string chan ";1"
  | BA_Underline  -> output_string chan ";4"
  | BA_Blink      -> output_string chan ";5"
  | BA_Negative   -> output_string chan ";7"
  | BA_CrossedOut -> output_string chan ";9"
  | BA_FgColor c  -> 
      output_string chan (";" ^ string_of_int (30 + (color_code c)))
  | BA_BgColor c  -> 
      output_string chan (";" ^ string_of_int (40 + (color_code c)))

let output_attr_code chan attr =
  match attr with
  | Error ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_error
  | Keyword ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_keyword
  | Number ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_number
  | Literal ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_literal
  | Constant ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_constant
  | Constructor ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_constructor
  | Operator ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_operator
  | Paren ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_paren
  | Variable ->
    List.iter (output_base_attr_code chan) (!current_scheme).s_variable
  | Base attr -> output_base_attr_code chan attr

let use_attributes chan attrs =
  output_string chan "\027[";
  List.iter (output_attr_code chan) attrs;
  output_char chan 'm'

let default_attributes chan =
  output_string chan "\027[0m"
